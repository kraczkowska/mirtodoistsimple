package ai.makeitright.tests.checktitleofpage;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class CheckTitleOfPage {

    private String URL;
    private WebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("inputParameters.url","http://mir-todoist-test-app.s3-website-us-east-1.amazonaws.com/");
    }

    @Before
    public void before() {
        URL = System.getProperty("inputParameters.url");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() {
            driver.get(URL);
            String pageTitle = driver.getTitle();
            JSONObject obj = new JSONObject();
            obj.put("titleofpage", pageTitle);
            System.setProperty("output", obj.toString());
            driver.close();
    }



}

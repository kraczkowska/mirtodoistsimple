package ai.makeitright.tests.quickaddtask;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

public class QuickAddTask {
    private String TASKNAME;
    private String URL;
    private WebDriver driver;

//    @BeforeClass
//    public static void beforeClass() {
//        System.setProperty("inputParameters.url","http://mir-todoist-test-app.s3-website-us-east-1.amazonaws.com/");
//        System.setProperty("inputParameters.taskname","Kasia task");
//    }

    @Before
    public void before() {
        TASKNAME = System.getProperty("inputParameters.taskname");
        URL = System.getProperty("inputParameters.url");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--single-process");
        options.addArguments("--use-gl=swiftshader");
        options.addArguments("--no-zygote");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @Test
    public void test() {
        driver.get(URL);
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected '" + URL + "'",URL,driver.getCurrentUrl());
        driver.findElement(By.xpath("//button[@data-testid='quick-add-task-action']")).click();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String taskName = TASKNAME + " " + formatter.format(new GregorianCalendar().getTime());
        driver.findElement(By.xpath("//input[@data-testid='add-task-content']")).sendKeys(taskName);

        driver.findElement(By.xpath("//button[@data-testid='add-task']")).click();

        Assert.assertTrue(isTaskAtInbox(taskName));

        JSONObject obj = new JSONObject();
        obj.put("taskname", taskName);
        System.setProperty("output", obj.toString());
        driver.close();

    }

    private boolean isTaskAtInbox(String taskName) {
        List<WebElement> tableRows = driver.findElements(By.xpath("//ul[@class='tasks__list']/li"));
        if(tableRows.size() > 0) {
            for(WebElement row : tableRows) {
                WebElement rowTaskName = row.findElement(By.xpath("./span"));
                if (rowTaskName.getText().equals(taskName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
